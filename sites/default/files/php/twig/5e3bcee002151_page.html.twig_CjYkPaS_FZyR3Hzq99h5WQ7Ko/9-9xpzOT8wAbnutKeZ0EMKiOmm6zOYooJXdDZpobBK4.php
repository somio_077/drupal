<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/yg_sports/templates/page.html.twig */
class __TwigTemplate_c05a436c57f0936c33e426639b0b70656e78654cf8dc688967aef39d1d801998 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'navbar' => [$this, 'block_navbar'],
            'main' => [$this, 'block_main'],
            'header' => [$this, 'block_header'],
            'sidebar_first' => [$this, 'block_sidebar_first'],
            'highlighted' => [$this, 'block_highlighted'],
            'breadcrumb' => [$this, 'block_breadcrumb'],
            'action_links' => [$this, 'block_action_links'],
            'help' => [$this, 'block_help'],
            'content' => [$this, 'block_content'],
            'sidebar_second' => [$this, 'block_sidebar_second'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 61, "block" => 62, "set" => 116];
        $filters = ["escape" => 176, "t" => 68];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'block', 'set'],
                ['escape', 't'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 59
        echo "<nav id=\"menu\" class=\"navbar navbar-default navbar-fixed-top\">
  <div class=\"container\">
";
        // line 61
        if (($this->getAttribute(($context["page"] ?? null), "navigation", []) || $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", []))) {
            // line 62
            echo "  ";
            $this->displayBlock('navbar', $context, $blocks);
        }
        // line 86
        echo "  </div>
</nav>


 ";
        // line 91
        $this->displayBlock('main', $context, $blocks);
        // line 171
        echo "<!-- <div id=\"join\" class=\"text-center\">
  <div class=\"container\">
    <div class=\"col-md-12\">
      <div class=\"row\">
        <div class=\"col-md-10\">
       <p>";
        // line 176
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["joinus_description"] ?? null)), "html", null, true);
        echo "</p>
        </div>
        <div class=\"col-md-2\">
          <a href='";
        // line 179
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["joinusurl"] ?? null)), "html", null, true);
        echo "' class=\"btn btn-custom center-block btn-lg\">JOIN US</a>
        </div>
      </div>
    </div>
  </div>
</div> -->
<div id=\"footer\">
  <div class=\"container text-center\">
    <p>&copy; 2017. All Rights Reserved.<br />Theme Powered By <a href=\"http://www.youngglobes.com\" target=\"_blank\">Young Globes</a></p>
  </div>
</div>
<a id=\"back-to-top\" href=\"#home\" class=\"btn btn-custom btn-lg page-scroll\" role=\"button\" title=\"Go To Top\"><span class=\"glyphicon glyphicon-chevron-up\"></span></a>
";
    }

    // line 62
    public function block_navbar($context, array $blocks = [])
    {
        // line 63
        echo "    <div class=\"navbar-header\">
        ";
        // line 64
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation", [])), "html", null, true);
        echo "
        ";
        // line 66
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 67
            echo "          <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
            <span class=\"sr-only\">";
            // line 68
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Toggle navigation"));
            echo "</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
          </button>
        ";
        }
        // line 74
        echo "        <a class=\"navbar-brand page-scroll\" href=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["front_page"] ?? null)), "html", null, true);
        echo "\"><img src=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["logopath"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"logo\" title=\"logo\"></a>
      </div>
      ";
        // line 77
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 78
            echo "    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
      <ul class=\"nav navbar-nav navbar-right\">
          ";
            // line 80
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])), "html", null, true);
            echo "
      </ul>
    </div>
      ";
        }
        // line 84
        echo "  ";
    }

    // line 91
    public function block_main($context, array $blocks = [])
    {
        // line 92
        echo "  <div role=\"main\" class=\"container blog-container js-quickedit-main-content\">
    <div class=\"row\">


      ";
        // line 97
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 98
            echo "        ";
            $this->displayBlock('header', $context, $blocks);
            // line 103
            echo "      ";
        }
        // line 104
        echo "
      ";
        // line 106
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 107
            echo "        ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 112
            echo "      ";
        }
        // line 113
        echo "
      ";
        // line 115
        echo "      ";
        // line 116
        $context["content_classes"] = [0 => ((($this->getAttribute(        // line 117
($context["page"] ?? null), "sidebar_first", []) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("col-sm-6") : ("")), 1 => ((($this->getAttribute(        // line 118
($context["page"] ?? null), "sidebar_first", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-9") : ("")), 2 => ((($this->getAttribute(        // line 119
($context["page"] ?? null), "sidebar_second", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty($this->getAttribute(        // line 120
($context["page"] ?? null), "sidebar_first", [])) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-12") : (""))];
        // line 123
        echo "      <section";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content_attributes"] ?? null), "addClass", [0 => ($context["content_classes"] ?? null)], "method")), "html", null, true);
        echo ">

        ";
        // line 126
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 127
            echo "          ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 130
            echo "        ";
        }
        // line 131
        echo "
        ";
        // line 133
        echo "        ";
        if (($context["breadcrumb"] ?? null)) {
            // line 134
            echo "          ";
            $this->displayBlock('breadcrumb', $context, $blocks);
            // line 137
            echo "        ";
        }
        // line 138
        echo "
        ";
        // line 140
        echo "        ";
        if (($context["action_links"] ?? null)) {
            // line 141
            echo "          ";
            $this->displayBlock('action_links', $context, $blocks);
            // line 144
            echo "        ";
        }
        // line 145
        echo "
        ";
        // line 147
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "help", [])) {
            // line 148
            echo "          ";
            $this->displayBlock('help', $context, $blocks);
            // line 151
            echo "        ";
        }
        // line 152
        echo "
        ";
        // line 154
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 158
        echo "      </section>

      ";
        // line 161
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 162
            echo "        ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 167
            echo "      ";
        }
        // line 168
        echo "   </div>
   </div>
";
    }

    // line 98
    public function block_header($context, array $blocks = [])
    {
        // line 99
        echo "          <div class=\"col-sm-12\" role=\"heading\">
            ";
        // line 100
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
          </div>
        ";
    }

    // line 107
    public function block_sidebar_first($context, array $blocks = [])
    {
        // line 108
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 109
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 127
    public function block_highlighted($context, array $blocks = [])
    {
        // line 128
        echo "            <div class=\"highlighted\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
        echo "</div>
          ";
    }

    // line 134
    public function block_breadcrumb($context, array $blocks = [])
    {
        // line 135
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["breadcrumb"] ?? null)), "html", null, true);
        echo "
          ";
    }

    // line 141
    public function block_action_links($context, array $blocks = [])
    {
        // line 142
        echo "            <ul class=\"action-links\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["action_links"] ?? null)), "html", null, true);
        echo "</ul>
          ";
    }

    // line 148
    public function block_help($context, array $blocks = [])
    {
        // line 149
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "help", [])), "html", null, true);
        echo "
          ";
    }

    // line 154
    public function block_content($context, array $blocks = [])
    {
        // line 155
        echo "          <a id=\"main-content\"></a>
          ";
        // line 156
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
        ";
    }

    // line 162
    public function block_sidebar_second($context, array $blocks = [])
    {
        // line 163
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 164
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    public function getTemplateName()
    {
        return "themes/yg_sports/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  365 => 164,  362 => 163,  359 => 162,  353 => 156,  350 => 155,  347 => 154,  340 => 149,  337 => 148,  330 => 142,  327 => 141,  320 => 135,  317 => 134,  310 => 128,  307 => 127,  300 => 109,  297 => 108,  294 => 107,  287 => 100,  284 => 99,  281 => 98,  275 => 168,  272 => 167,  269 => 162,  266 => 161,  262 => 158,  259 => 154,  256 => 152,  253 => 151,  250 => 148,  247 => 147,  244 => 145,  241 => 144,  238 => 141,  235 => 140,  232 => 138,  229 => 137,  226 => 134,  223 => 133,  220 => 131,  217 => 130,  214 => 127,  211 => 126,  205 => 123,  203 => 120,  202 => 119,  201 => 118,  200 => 117,  199 => 116,  197 => 115,  194 => 113,  191 => 112,  188 => 107,  185 => 106,  182 => 104,  179 => 103,  176 => 98,  173 => 97,  167 => 92,  164 => 91,  160 => 84,  153 => 80,  149 => 78,  146 => 77,  138 => 74,  129 => 68,  126 => 67,  123 => 66,  119 => 64,  116 => 63,  113 => 62,  96 => 179,  90 => 176,  83 => 171,  81 => 91,  75 => 86,  71 => 62,  69 => 61,  65 => 59,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/yg_sports/templates/page.html.twig", "E:\\XAMPP\\htdocs\\mydrupal\\themes\\yg_sports\\templates\\page.html.twig");
    }
}
