<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/yg_sports/templates/page--front.html.twig */
class __TwigTemplate_892f1da021c0915a6f29fa966420ef7b6a06ed5cd068a778fed61f5fc2e3285a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'navbar' => [$this, 'block_navbar'],
            'main' => [$this, 'block_main'],
            'header' => [$this, 'block_header'],
            'sidebar_first' => [$this, 'block_sidebar_first'],
            'highlighted' => [$this, 'block_highlighted'],
            'breadcrumb' => [$this, 'block_breadcrumb'],
            'action_links' => [$this, 'block_action_links'],
            'help' => [$this, 'block_help'],
            'content' => [$this, 'block_content'],
            'sidebar_second' => [$this, 'block_sidebar_second'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 3, "block" => 4, "for" => 136, "set" => 51];
        $filters = ["escape" => 105, "t" => 10];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'block', 'for', 'set'],
                ['escape', 't'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<nav id=\"menu\" class=\"navbar navbar-default navbar-fixed-top\">
  <div class=\"container\">
";
        // line 3
        if (($this->getAttribute(($context["page"] ?? null), "navigation", []) || $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", []))) {
            // line 4
            echo "  ";
            $this->displayBlock('navbar', $context, $blocks);
        }
        // line 27
        echo "  </div>
</nav>
";
        // line 30
        $this->displayBlock('main', $context, $blocks);
        // line 100
        echo "<div id=\"join\" class=\"text-center\">
  <div class=\"container\">
    <div class=\"col-md-12\">
      <div class=\"row\">
        <div class=\"col-md-10 col-sm-8 text-left\">
       <p>";
        // line 105
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["joinus_description"] ?? null)), "html", null, true);
        echo "</p>
        </div>
        <div class=\"col-md-2 col-sm-4\">
          <a href='";
        // line 108
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["joinusurl"] ?? null)), "html", null, true);
        echo "' class=\"btn btn-custom center-block btn-lg\">JOIN US</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div id=\"contact\" class=\"text-center\">
  <div class=\"container\">
    <div class=\"col-md-12\">
      <div class=\"row\">
        <div class=\"section-title\">
          <h2>Contact</h2>
        </div>
        <div class=\"col-md-4\">
        ";
        // line 122
        if ($this->getAttribute(($context["page"] ?? null), "footer_left", [])) {
            // line 123
            echo "          ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_left", [])), "html", null, true);
            echo "
        ";
        }
        // line 125
        echo "        </div>
        <div class=\"col-md-6\">
        ";
        // line 127
        if ($this->getAttribute(($context["page"] ?? null), "footer_middle", [])) {
            // line 128
            echo "          <div id=\"map\">
            ";
            // line 129
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_middle", [])), "html", null, true);
            echo "
          </div>
        ";
        }
        // line 132
        echo "        </div>
    <div class=\"col-md-2 text-left call-us\">
        <div class=\"widget col-md-12 col-sm-4 col-xs-12\">
        <h2>Call Us</h2>
        ";
        // line 136
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["phone_numbers"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["number"]) {
            // line 137
            echo "          <h4>";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["number"]), "html", null, true);
            echo "</h4>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['number'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 139
        echo "      </div>
      <div class=\"widget col-md-12 col-sm-4 col-xs-12\">
        <h2>Mail Us</h2>
        ";
        // line 142
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["emails"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["email"]) {
            // line 143
            echo "          <h4><a href=\"mailto:";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["email"]), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["email"]), "html", null, true);
            echo "</a></h4>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['email'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 145
        echo "      </div>
      <div class=\"widget col-md-12 col-sm-4 col-xs-12\">
        <h2>Follow Us</h2>
        <div class=\"social-links\">
        <a href=\"";
        // line 149
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook_url"] ?? null)), "html", null, true);
        echo "\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a>
        <a href=\"";
        // line 150
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter_url"] ?? null)), "html", null, true);
        echo "\" target=\"_blank\"><i class=\"fa fa-twitter\"></i></a>
        <a href=\"";
        // line 151
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["linkedin"] ?? null)), "html", null, true);
        echo "\" target=\"_blank\"><i class=\"fa fa-linkedin\"></i></a>
        </div>
      </div>
    </div>
      </div>
    </div>
  </div>
</div>
<div id=\"footer\">
  ";
        // line 160
        if ($this->getAttribute(($context["page"] ?? null), "footer", [])) {
            // line 161
            echo "    ";
            $this->displayBlock('footer', $context, $blocks);
            // line 166
            echo "  ";
        }
        // line 167
        echo "  <div class=\"container text-center\">
    <p>&copy; 2017. All Rights Reserved.<br />Theme Powered By <a href=\"http://www.youngglobes.com\" target=\"_blank\">Young Globes</a></p>
  </div>
</div>
<a id=\"back-to-top\" href=\"#home\" class=\"btn btn-custom page-scroll\" role=\"button\" title=\"Go To Top\"><span class=\"glyphicon glyphicon-chevron-up\"></span></a>
";
    }

    // line 4
    public function block_navbar($context, array $blocks = [])
    {
        // line 5
        echo "    <div class=\"navbar-header\">
        ";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation", [])), "html", null, true);
        echo "
        ";
        // line 8
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 9
            echo "          <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
            <span class=\"sr-only\">";
            // line 10
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Toggle navigation"));
            echo "</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
          </button>
        ";
        }
        // line 16
        echo "        <a class=\"navbar-brand page-scroll\" href=\"#home\"><img src=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["logopath"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"logo\" title=\"logo\"></a>
      </div>
      ";
        // line 19
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 20
            echo "    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
      
          ";
            // line 22
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])), "html", null, true);
            echo "
    </div>
      ";
        }
        // line 25
        echo "  ";
    }

    // line 30
    public function block_main($context, array $blocks = [])
    {
        // line 31
        echo "  <div role=\"main\" class=\"blog-container js-quickedit-main-content\">
    <div class=\"\">
      ";
        // line 34
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 35
            echo "        ";
            $this->displayBlock('header', $context, $blocks);
            // line 40
            echo "      ";
        }
        // line 41
        echo "      ";
        // line 42
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 43
            echo "        ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 48
            echo "      ";
        }
        // line 49
        echo "      ";
        // line 50
        echo "      ";
        // line 51
        $context["content_classes"] = [0 => ((($this->getAttribute(        // line 52
($context["page"] ?? null), "sidebar_first", []) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("col-sm-6") : ("")), 1 => ((($this->getAttribute(        // line 53
($context["page"] ?? null), "sidebar_first", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-9") : ("")), 2 => ((($this->getAttribute(        // line 54
($context["page"] ?? null), "sidebar_second", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty($this->getAttribute(        // line 55
($context["page"] ?? null), "sidebar_first", [])) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-12") : (""))];
        // line 58
        echo "      <section>
        ";
        // line 60
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 61
            echo "          ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 64
            echo "        ";
        }
        // line 65
        echo "        ";
        // line 66
        echo "        ";
        if (($context["breadcrumb"] ?? null)) {
            // line 67
            echo "          ";
            $this->displayBlock('breadcrumb', $context, $blocks);
            // line 70
            echo "        ";
        }
        // line 71
        echo "        ";
        // line 72
        echo "        ";
        if (($context["action_links"] ?? null)) {
            // line 73
            echo "          ";
            $this->displayBlock('action_links', $context, $blocks);
            // line 76
            echo "        ";
        }
        // line 77
        echo "        ";
        // line 78
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "help", [])) {
            // line 79
            echo "          ";
            $this->displayBlock('help', $context, $blocks);
            // line 82
            echo "        ";
        }
        // line 83
        echo "        ";
        // line 84
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 88
        echo "      </section>
      ";
        // line 90
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 91
            echo "        ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 96
            echo "      ";
        }
        // line 97
        echo "   </div>
 </div>
";
    }

    // line 35
    public function block_header($context, array $blocks = [])
    {
        // line 36
        echo "          <div class=\"col-sm-12\" role=\"heading\">
            ";
        // line 37
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
          </div>
        ";
    }

    // line 43
    public function block_sidebar_first($context, array $blocks = [])
    {
        // line 44
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 45
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 61
    public function block_highlighted($context, array $blocks = [])
    {
        // line 62
        echo "            <div class=\"highlighted\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
        echo "</div>
          ";
    }

    // line 67
    public function block_breadcrumb($context, array $blocks = [])
    {
        // line 68
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["breadcrumb"] ?? null)), "html", null, true);
        echo "
          ";
    }

    // line 73
    public function block_action_links($context, array $blocks = [])
    {
        // line 74
        echo "            <ul class=\"action-links\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["action_links"] ?? null)), "html", null, true);
        echo "</ul>
          ";
    }

    // line 79
    public function block_help($context, array $blocks = [])
    {
        // line 80
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "help", [])), "html", null, true);
        echo "
          ";
    }

    // line 84
    public function block_content($context, array $blocks = [])
    {
        // line 85
        echo "          <a id=\"main-content\"></a>
          ";
        // line 86
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
        ";
    }

    // line 91
    public function block_sidebar_second($context, array $blocks = [])
    {
        // line 92
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 93
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 161
    public function block_footer($context, array $blocks = [])
    {
        // line 162
        echo "      <footer class=\"footer ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\" role=\"contentinfo\">
        ";
        // line 163
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
        echo "
      </footer>
    ";
    }

    public function getTemplateName()
    {
        return "themes/yg_sports/templates/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  469 => 163,  464 => 162,  461 => 161,  454 => 93,  451 => 92,  448 => 91,  442 => 86,  439 => 85,  436 => 84,  429 => 80,  426 => 79,  419 => 74,  416 => 73,  409 => 68,  406 => 67,  399 => 62,  396 => 61,  389 => 45,  386 => 44,  383 => 43,  376 => 37,  373 => 36,  370 => 35,  364 => 97,  361 => 96,  358 => 91,  355 => 90,  352 => 88,  349 => 84,  347 => 83,  344 => 82,  341 => 79,  338 => 78,  336 => 77,  333 => 76,  330 => 73,  327 => 72,  325 => 71,  322 => 70,  319 => 67,  316 => 66,  314 => 65,  311 => 64,  308 => 61,  305 => 60,  302 => 58,  300 => 55,  299 => 54,  298 => 53,  297 => 52,  296 => 51,  294 => 50,  292 => 49,  289 => 48,  286 => 43,  283 => 42,  281 => 41,  278 => 40,  275 => 35,  272 => 34,  268 => 31,  265 => 30,  261 => 25,  255 => 22,  251 => 20,  248 => 19,  242 => 16,  233 => 10,  230 => 9,  227 => 8,  223 => 6,  220 => 5,  217 => 4,  208 => 167,  205 => 166,  202 => 161,  200 => 160,  188 => 151,  184 => 150,  180 => 149,  174 => 145,  163 => 143,  159 => 142,  154 => 139,  145 => 137,  141 => 136,  135 => 132,  129 => 129,  126 => 128,  124 => 127,  120 => 125,  114 => 123,  112 => 122,  95 => 108,  89 => 105,  82 => 100,  80 => 30,  76 => 27,  72 => 4,  70 => 3,  66 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/yg_sports/templates/page--front.html.twig", "E:\\XAMPP\\htdocs\\mydrupal\\themes\\yg_sports\\templates\\page--front.html.twig");
    }
}
